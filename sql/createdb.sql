USE p1;

-- CREATE Tables

CREATE TABLE people(
	person_id VARCHAR(5) NOT NULL,
	name VARCHAR(40) NOT NULL,
	birthday DATE,
	gender CHAR(1),
	birthplace VARCHAR(15),
	attribute VARCHAR(8),
	PRIMARY KEY(person_id)
);

CREATE TABLE imdb_user(
	user_id VARCHAR(6) NOT NULL,
	email VARCHAR(30) NOT NULL,
	first_name VARCHAR(20) NOT NULL,
	last_name VARCHAR(20),
	date_of_birth DATE,
	birthplace VARCHAR(15),
	gender CHAR(1),
	PRIMARY KEY(user_id)
);

CREATE TABLE movies(
	movie_id VARCHAR(5) NOT NULL,
	title VARCHAR(60) NOT NULL,
	release_year YEAR,
	director_id VARCHAR(5) NOT NULL,
	PRIMARY KEY(movie_id),
	FOREIGN KEY(director_id) REFERENCES people(person_id) ON DELETE CASCADE
);

CREATE TABLE movie_genre(
	movie_id VARCHAR(5) NOT NULL,
	genre VARCHAR(15) NOT NULL,
	FOREIGN KEY(movie_id) REFERENCES movies(movie_id) ON DELETE CASCADE
);

CREATE TABLE roles(
	movie_id VARCHAR(5) NOT NULL,
	person_id VARCHAR(5) NOT NULL,
	role VARCHAR(40) NOT NULL,
	FOREIGN KEY(movie_id) REFERENCES movies(movie_id) ON DELETE CASCADE,
	FOREIGN KEY(person_id) REFERENCES people(person_id) ON DELETE CASCADE
);

CREATE TABLE reviews(
	movie_id VARCHAR(5) NOT NULL,
	author_id VARCHAR(6) NOT NULL,
	rating SMALLINT CHECK(rating >= 0 AND rating <= 10),
	votes SMALLINT CHECK(votes >= 0),
	publish_date VARCHAR(24),
	FOREIGN KEY(movie_id) REFERENCES movies(movie_id) ON DELETE CASCADE,
	FOREIGN KEY(author_id) REFERENCES imdb_user(user_id) ON DELETE CASCADE
);

-- POPULATE Data

INSERT INTO people(person_id, name, birthday, gender, birthplace, attribute) VALUES('P1', 'Brian de forma', STR_TO_DATE('1940-09-11', '%Y-%m-%d'), 'M', 'New York', 'Director');
INSERT INTO people(person_id, name, birthday, gender, birthplace, attribute) VALUES('P2', 'Martin Brest', STR_TO_DATE('1951-08-08', '%Y-%m-%d'), 'M', 'San Jose', 'Director');
INSERT INTO people(person_id, name, birthday, gender, birthplace, attribute) VALUES('P3', 'Scarlett Johanson', STR_TO_DATE('1984-11-22', '%Y-%m-%d'), 'F', 'Austin', 'Actor');
INSERT INTO people(person_id, name, birthday, gender, birthplace, attribute) VALUES('P4', 'Luc Besson', STR_TO_DATE('1975-05-30', '%Y-%m-%d'), 'F', 'Paris', 'Director');
INSERT INTO people(person_id, name, birthday, gender, birthplace, attribute) VALUES('P5', 'Morgan Freeman', STR_TO_DATE('1953-06-05', '%Y-%m-%d'), 'M', 'Canberra', 'Actor');
INSERT INTO people(person_id, name, birthday, gender, birthplace, attribute) VALUES('P6', 'Al Pacino', STR_TO_DATE('1956-11-12', '%Y-%m-%d'), 'M', 'Portland', 'Actor');
INSERT INTO people(person_id, name, birthday, gender, birthplace, attribute) VALUES('P7', 'Angelina Jolie', STR_TO_DATE('1970-03-03', '%Y-%m-%d'), 'F', 'Seattle', 'Actor');
INSERT INTO people(person_id, name, birthday, gender, birthplace, attribute) VALUES('P8', 'Brad Pitt', STR_TO_DATE('1975-04-04', '%Y-%m-%d'), 'M', 'London', 'Actor');
INSERT INTO people(person_id, name, birthday, gender, birthplace, attribute) VALUES('P9', 'Tom Hanks', STR_TO_DATE('1964-05-19', '%Y-%m-%d'), 'M', 'Perth', 'Actor');
INSERT INTO people(person_id, name, birthday, gender, birthplace, attribute) VALUES('P10', 'Jessica Alba', STR_TO_DATE('1983-08-07', '%Y-%m-%d'), 'F', 'Seoul', 'Actor');
INSERT INTO people(person_id, name, birthday, gender, birthplace, attribute) VALUES('P12', 'Alex Parish', STR_TO_DATE('1977-07-09', '%Y-%m-%d'), 'F', 'San Jose', 'Actor');
INSERT INTO people(person_id, name, birthday, gender, birthplace, attribute) VALUES('P13', 'Jack Nicholson', STR_TO_DATE('1958-11-13', '%Y-%m-%d'), 'M', 'Austin', 'Actor');
INSERT INTO people(person_id, name, birthday, gender, birthplace, attribute) VALUES('P15', 'Harrison Ford', STR_TO_DATE('1957-09-11', '%Y-%m-%d'), 'M', 'Canberra', 'Actor');
INSERT INTO people(person_id, name, birthday, gender, birthplace, attribute) VALUES('P16', 'Julia Roberts', STR_TO_DATE('1967-01-01', '%Y-%m-%d'), 'F', 'Portland', 'Actor');
INSERT INTO people(person_id, name, birthday, gender, birthplace, attribute) VALUES('P17', 'Matt Damon', STR_TO_DATE('1971-01-07', '%Y-%m-%d'), 'M', 'Seattle', 'Actor');
INSERT INTO people(person_id, name, birthday, gender, birthplace, attribute) VALUES('P18', 'Jennifer Lawrence', STR_TO_DATE('1962-02-02', '%Y-%m-%d'), 'F', 'London', 'Actor');
INSERT INTO people(person_id, name, birthday, gender, birthplace, attribute) VALUES('P19', 'George clooney', STR_TO_DATE('1965-03-03', '%Y-%m-%d'), 'M', 'Perth', 'Actor');
INSERT INTO people(person_id, name, birthday, gender, birthplace, attribute) VALUES('P20', 'Jennifer Aniston', STR_TO_DATE('1968-04-04', '%Y-%m-%d'), 'F', 'Seoul', 'Actor');
INSERT INTO people(person_id, name, birthday, gender, birthplace, attribute) VALUES('P420', 'Hucha Venkat', STR_TO_DATE('1969-04-04', '%Y-%m-%d'), 'M', 'Mandya', 'Actor');

INSERT INTO imdb_user(user_id, email, first_name, last_name, date_of_birth, birthplace, gender) VALUES('ID1', 'john@yahoo.com', 'John', 'Smith', STR_TO_DATE('1995-10-05', '%Y-%m-%d'), 'FL', 'M');
INSERT INTO imdb_user(user_id, email, first_name, last_name, date_of_birth, birthplace, gender) VALUES('ID2', 'juan@gmail.com', 'Juan', 'Carlos', STR_TO_DATE('1994-04-12', '%Y-%m-%d'), 'AK', 'M');
INSERT INTO imdb_user(user_id, email, first_name, last_name, date_of_birth, birthplace, gender) VALUES('ID3', 'Jane@gmail.com', 'Jane', 'Chapel', STR_TO_DATE('1993-11-02', '%Y-%m-%d'), 'IL', 'F');
INSERT INTO imdb_user(user_id, email, first_name, last_name, date_of_birth, birthplace, gender) VALUES('ID4', 'adi@yahoo.com', 'Aditya', 'Awasthi', STR_TO_DATE('1992-12-12', '%Y-%m-%d'), 'CA', 'M');
INSERT INTO imdb_user(user_id, email, first_name, last_name, date_of_birth, birthplace, gender) VALUES('ID5', 'james@hotmail.com', 'James', 'Williams', STR_TO_DATE('1991-05-05', '%Y-%m-%d'), 'NY', 'M');
INSERT INTO imdb_user(user_id, email, first_name, last_name, date_of_birth, birthplace, gender) VALUES('ID6', 'mike@yahoo.com', 'Mike', 'Brown', STR_TO_DATE('1988-03-01', '%Y-%m-%d'), 'NC', 'M');
INSERT INTO imdb_user(user_id, email, first_name, last_name, date_of_birth, birthplace, gender) VALUES('ID7', 'bob@yahoo.com', 'Bob', 'Jones', STR_TO_DATE('1988-02-07', '%Y-%m-%d'), 'NY ', 'M');
INSERT INTO imdb_user(user_id, email, first_name, last_name, date_of_birth, birthplace, gender) VALUES('ID8', 'wei@gmail.com', ' Wei', 'Zhang', STR_TO_DATE('1985-08-12', '%Y-%m-%d'), 'NV', 'F');
INSERT INTO imdb_user(user_id, email, first_name, last_name, date_of_birth, birthplace, gender) VALUES('ID9', 'mark@gmail.com', 'Mark', 'Davis', STR_TO_DATE('1984-05-10', '%Y-%m-%d'), 'CA', 'M');
INSERT INTO imdb_user(user_id, email, first_name, last_name, date_of_birth, birthplace, gender) VALUES('ID10', 'daniel@yahoo.com', 'Daniel ', 'Garcia', STR_TO_DATE('1980-06-01', '%Y-%m-%d'), 'NJ', 'M');
INSERT INTO imdb_user(user_id, email, first_name, last_name, date_of_birth, birthplace, gender) VALUES('ID11', 'maria@hotmail.com', 'Maria', 'Rodriguez', STR_TO_DATE('1975-03-18', '%Y-%m-%d'), 'CA', 'F');
INSERT INTO imdb_user(user_id, email, first_name, last_name, date_of_birth, birthplace, gender) VALUES('ID12', 'freya@yahoo.com', 'Freya', 'Wilson', STR_TO_DATE('1970-02-19', '%Y-%m-%d'), 'NJ', 'F');

INSERT INTO movies(movie_id, title, release_year, director_id) VALUES('M1', 'Scarface', 1988, 'P1');
INSERT INTO movies(movie_id, title, release_year, director_id) VALUES('M2', 'Scent of a women', 1995, 'P2');
INSERT INTO movies(movie_id, title, release_year, director_id) VALUES('M3', 'My big fat greek wedding', 2000, 'P4');
INSERT INTO movies(movie_id, title, release_year, director_id) VALUES('M4', 'The Devil''s Advocate', 1997, 'P1');
INSERT INTO movies(movie_id, title, release_year, director_id) VALUES('M5', 'Mr. and Mrs Smith', 1965, 'P1');
INSERT INTO movies(movie_id, title, release_year, director_id) VALUES('M6', 'Now You see me', 2013, 'P2');
INSERT INTO movies(movie_id, title, release_year, director_id) VALUES('M7', 'Barely Lethal', 2014, 'P4');
INSERT INTO movies(movie_id, title, release_year, director_id) VALUES('M8', 'The Man with one red shoe', 1984, 'P1');
INSERT INTO movies(movie_id, title, release_year, director_id) VALUES('M9', 'The Polar Express', 2010, 'P2');
INSERT INTO movies(movie_id, title, release_year, director_id) VALUES('M10', 'Her', 2013, 'P2');
INSERT INTO movies(movie_id, title, release_year, director_id) VALUES('M11', 'Lucy', 2015, 'P4');
INSERT INTO movies(movie_id, title, release_year, director_id) VALUES('M12', 'The Da Vinci Code', 2005, 'P4');
INSERT INTO movies(movie_id, title, release_year, director_id) VALUES('M13', 'The God Father part II', 1975, 'P1');
INSERT INTO movies(movie_id, title, release_year, director_id) VALUES('M15', 'Angels and Daemons', 2009, 'P2');
INSERT INTO movies(movie_id, title, release_year, director_id) VALUES('M16', 'The Island', 2010, 'P4');
INSERT INTO movies(movie_id, title, release_year, director_id) VALUES('M420', 'Dummy Movie', 2010, 'P420');

INSERT INTO movie_genre(movie_id, genre) VALUES('M1', 'Action');
INSERT INTO movie_genre(movie_id, genre) VALUES('M2', 'Action');
INSERT INTO movie_genre(movie_id, genre) VALUES('M2', 'Comedy');
INSERT INTO movie_genre(movie_id, genre) VALUES('M3', 'Comedy');
INSERT INTO movie_genre(movie_id, genre) VALUES('M4', 'Thriller');
INSERT INTO movie_genre(movie_id, genre) VALUES('M5', 'Comedy');
INSERT INTO movie_genre(movie_id, genre) VALUES('M5', 'Action');
INSERT INTO movie_genre(movie_id, genre) VALUES('M6', 'Thriller');
INSERT INTO movie_genre(movie_id, genre) VALUES('M7', 'Action');
INSERT INTO movie_genre(movie_id, genre) VALUES('M8', 'comedy');
INSERT INTO movie_genre(movie_id, genre) VALUES('M9', 'comedy');
INSERT INTO movie_genre(movie_id, genre) VALUES('M10', 'Thriller');
INSERT INTO movie_genre(movie_id, genre) VALUES('M11', 'Thriller');
INSERT INTO movie_genre(movie_id, genre) VALUES('M12', 'Action');
INSERT INTO movie_genre(movie_id, genre) VALUES('M12', 'Thriller');
INSERT INTO movie_genre(movie_id, genre) VALUES('M13', 'Action');
INSERT INTO movie_genre(movie_id, genre) VALUES('M13', 'Thriller');
INSERT INTO movie_genre(movie_id, genre) VALUES('M15', 'Action');
INSERT INTO movie_genre(movie_id, genre) VALUES('M15', 'Thriller');
INSERT INTO movie_genre(movie_id, genre) VALUES('M16', 'Action');
INSERT INTO movie_genre(movie_id, genre) VALUES('M16', 'Comedy');
INSERT INTO movie_genre(movie_id, genre) VALUES('M420', 'Tragedy');

INSERT INTO roles(movie_id, person_id, role) VALUES('M1', 'P5', 'Jessica');
INSERT INTO roles(movie_id, person_id, role) VALUES('M1', 'P6', 'robert');
INSERT INTO roles(movie_id, person_id, role) VALUES('M2', 'P5', 'John');
INSERT INTO roles(movie_id, person_id, role) VALUES('M2', 'P6', 'Mark');
INSERT INTO roles(movie_id, person_id, role) VALUES('M3', 'P9', 'Alex');
INSERT INTO roles(movie_id, person_id, role) VALUES('M3', 'P7', 'Julie');
INSERT INTO roles(movie_id, person_id, role) VALUES('M4', 'P5', 'Jessica');
INSERT INTO roles(movie_id, person_id, role) VALUES('M4', 'P6', 'Matt');
INSERT INTO roles(movie_id, person_id, role) VALUES('M4', 'P8', 'Jennifer');
INSERT INTO roles(movie_id, person_id, role) VALUES('M5', 'P7', 'Jennifer');
INSERT INTO roles(movie_id, person_id, role) VALUES('M5', 'P8', 'Tom');
INSERT INTO roles(movie_id, person_id, role) VALUES('M5', 'P5', 'Milo');
INSERT INTO roles(movie_id, person_id, role) VALUES('M6', 'P6', 'Chris');
INSERT INTO roles(movie_id, person_id, role) VALUES('M6', 'P7', 'Rose');
INSERT INTO roles(movie_id, person_id, role) VALUES('M6', 'P5', 'Bill');
INSERT INTO roles(movie_id, person_id, role) VALUES('M7', 'P10', 'Jane');
INSERT INTO roles(movie_id, person_id, role) VALUES('M7', 'P5', 'Brad');
INSERT INTO roles(movie_id, person_id, role) VALUES('M8', 'P9', 'Lucas');
INSERT INTO roles(movie_id, person_id, role) VALUES('M8', 'P10', 'Juanita');
INSERT INTO roles(movie_id, person_id, role) VALUES('M9', 'P9', 'Clayne');
INSERT INTO roles(movie_id, person_id, role) VALUES('M9', 'P9', 'Jane');
INSERT INTO roles(movie_id, person_id, role) VALUES('M9', 'P9', 'Brad');
INSERT INTO roles(movie_id, person_id, role) VALUES('M9', 'P9', 'Lucas');
INSERT INTO roles(movie_id, person_id, role) VALUES('M9', 'P9', 'Bradley');
INSERT INTO roles(movie_id, person_id, role) VALUES('M9', 'P9', 'Justin');
INSERT INTO roles(movie_id, person_id, role) VALUES('M9', 'P17', 'Martin');
INSERT INTO roles(movie_id, person_id, role) VALUES('M9', 'P19', 'Mike');
INSERT INTO roles(movie_id, person_id, role) VALUES('M10', 'P3', 'Travis');
INSERT INTO roles(movie_id, person_id, role) VALUES('M10', 'P5', 'Alexander');
INSERT INTO roles(movie_id, person_id, role) VALUES('M10', 'P6', 'Justin');
INSERT INTO roles(movie_id, person_id, role) VALUES('M11', 'P3', 'Jessica');
INSERT INTO roles(movie_id, person_id, role) VALUES('M11', 'P5', 'Johnny');
INSERT INTO roles(movie_id, person_id, role) VALUES('M11', 'P8', 'Rami');
INSERT INTO roles(movie_id, person_id, role) VALUES('M11', 'P9', 'Sam');
INSERT INTO roles(movie_id, person_id, role) VALUES('M12', 'P9', 'Gatek');
INSERT INTO roles(movie_id, person_id, role) VALUES('M12', 'P10', 'Rose');
INSERT INTO roles(movie_id, person_id, role) VALUES('M12', 'P3', 'maria');
INSERT INTO roles(movie_id, person_id, role) VALUES('M13', 'P5', 'Travis');
INSERT INTO roles(movie_id, person_id, role) VALUES('M13', 'P6', 'Alexander');
INSERT INTO roles(movie_id, person_id, role) VALUES('M13', 'P16', 'Pearl');
INSERT INTO roles(movie_id, person_id, role) VALUES('M15', 'P12', 'Sofia');
INSERT INTO roles(movie_id, person_id, role) VALUES('M15', 'P18', 'chrissy');
INSERT INTO roles(movie_id, person_id, role) VALUES('M15', 'P9', 'Mike');
INSERT INTO roles(movie_id, person_id, role) VALUES('M16', 'P10', 'Martin');
INSERT INTO roles(movie_id, person_id, role) VALUES('M16', 'P15', 'Bill');
INSERT INTO roles(movie_id, person_id, role) VALUES('M16', 'P16', 'Emilia');
INSERT INTO roles(movie_id, person_id, role) VALUES('M420', 'P420', 'Self');

INSERT INTO reviews(movie_id, author_id, rating, votes, publish_date) VALUES('M1', 'ID1', 7, 25, '2007-10-02 09:10:54');
INSERT INTO reviews(movie_id, author_id, rating, votes, publish_date) VALUES('M2', 'ID2', 8, 35, '2007-09-29 13:45:00');
INSERT INTO reviews(movie_id, author_id, rating, votes, publish_date) VALUES('M2', 'ID3', 9, 24, '2007-09-29 10:38:25');
INSERT INTO reviews(movie_id, author_id, rating, votes, publish_date) VALUES('M3', 'ID4', 10, 8, '2013-10-02 13:05:56');
INSERT INTO reviews(movie_id, author_id, rating, votes, publish_date) VALUES('M3', 'ID5', 9, 11, '2007-10-25 17:15:00');
INSERT INTO reviews(movie_id, author_id, rating, votes, publish_date) VALUES('M4', 'ID6', 8, 6, '2007-09-26 17:15:00');
INSERT INTO reviews(movie_id, author_id, rating, votes, publish_date) VALUES('M4', 'ID7', 7, 23, '2007-09-26 17:15:00');
INSERT INTO reviews(movie_id, author_id, rating, votes, publish_date) VALUES('M5', 'ID9', 9, 22, '2007-09-28 17:15:00');
INSERT INTO reviews(movie_id, author_id, rating, votes, publish_date) VALUES('M6', 'ID10', 8, 26, '2007-10-29 17:15:00');
INSERT INTO reviews(movie_id, author_id, rating, votes, publish_date) VALUES('M7', 'ID11', 8, 27, '2007-09-30 17:15:00');
INSERT INTO reviews(movie_id, author_id, rating, votes, publish_date) VALUES('M7', 'ID12', 8, 18, '2007-10-25 17:15:00');
INSERT INTO reviews(movie_id, author_id, rating, votes, publish_date) VALUES('M8', 'ID1', 7, 19, '2007-09-25 17:15:00');
INSERT INTO reviews(movie_id, author_id, rating, votes, publish_date) VALUES('M9', 'ID2', 7, 16, '2007-09-25 17:15:00');
INSERT INTO reviews(movie_id, author_id, rating, votes, publish_date) VALUES('M10', 'ID3', 8, 18, '2007-09-29 17:15:00');
INSERT INTO reviews(movie_id, author_id, rating, votes, publish_date) VALUES('M11', 'ID4', 9, 22, '2015-06-07 17:15:00');
INSERT INTO reviews(movie_id, author_id, rating, votes, publish_date) VALUES('M11', 'ID5', 10, 13, '2015-05-05 17:15:00');
INSERT INTO reviews(movie_id, author_id, rating, votes, publish_date) VALUES('M12', 'ID6', 9, 50, '2015-05-05 17:15:00');
INSERT INTO reviews(movie_id, author_id, rating, votes, publish_date) VALUES('M13', 'ID7', 5, 34, '2007-10-25 17:15:00');
INSERT INTO reviews(movie_id, author_id, rating, votes, publish_date) VALUES('M13', 'ID1', 4, 34, '2007-10-25 17:15:00');
INSERT INTO reviews(movie_id, author_id, rating, votes, publish_date) VALUES('M15', 'ID10', 8, 25, '2015-05-05 17:15:00');
INSERT INTO reviews(movie_id, author_id, rating, votes, publish_date) VALUES('M16', 'ID11', 7, 12, '2015-05-05 17:15:00');