USE p1;

-- DROP Tables

DROP TABLE roles;
DROP TABLE reviews;
DROP TABLE movie_genre;

DROP TABLE movies;
DROP TABLE imdb_user;
DROP TABLE people;