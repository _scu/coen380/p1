# COEN380 Group Project 1

## Project Overview
Each student will create an RDBMS personal development environment, e.g., using their own laptop or an alternative. The purpose of this project is to demonstrate your ability to set up the RDBMS environment and creating very simple schema (one or two tables) and exercise select and IUD (Insert/Update/Delete) operations on the database in both interactive mode as well as using JDBC successfully.

1. Select you favorite RDBMS; ensure that it supports JDBC
1. Download and install both the RDBMS and JDBC driver
1. Create a Database with a schema (example, sales transactions) that consists of one table. Make sure that you have a PK (one column – customer-id) in that table.
1. Populate the table (Insert SQL statements) with many entries representing sales transactions
1. Query the table with different flavors of SELECT statement including Join, Aggregate functions, ORDER BY, GROUP BY, etc.
1. Submit a report that lists:

	1. Database used
	1. JDBC driver (type-4)
	1. The table schema definition